# Developer note

## Usecase 1: Add in new export method such as to export as JSON

1. Navigate to `src/export/`, create a file, for example `export_json.py`
2. In the file, create your class and make sure need to import the interface `IExportAlgorithm` from `interface4export.py`, then implement the function `save_file()` as defined in the interface.

   ```py export_json.py
   from .interface4export import IExportAlgorithm


   class ExportJson(IExportAlgorithm):

      def save_file(self, data, filepath: str):
         ## Read, process the data and store it
         ...

   ```

3. Next register the function in the factory `factory4export.py`

   ```py factory4export.py
   from .export_json import ExportJson

   class ChooseExport(Enum):
      ...
      EX_JSON: str = "EX_JSON"

   # Factory class
   class ExportAlgorithmFactory:

      @staticmethod
      def get_exporter(file_type: ChooseExport):
         if file_type == ChooseExport.EX_PDF:
            ...
         elif file_type == ChooseExport.EX_JSON:
            return ExportJson()
         else:
            ...

   ```

4. Now developer may use the `ExportAlgorithmFactory` together with `ChooseExport` to select export method needed for your app!

   ```py
   json_exporter = ExportAlgorithmFactory.get_exporter(ChooseExport.EX_JSON)
   json_exporter.save_file(data, "output/data.json")
   ```
