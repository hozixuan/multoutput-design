def get_data(file_path):
    with open(file_path, mode="r") as fd:
        raw_texts = fd.readlines()

    data = [raw_text.strip() for raw_text in raw_texts]
    return data
