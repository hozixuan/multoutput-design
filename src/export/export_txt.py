from .interface4export import IExportAlgorithm


class ExportTxt(IExportAlgorithm):

    def save_file(self, data, filepath: str):
        with open(filepath, "w") as file:
            file.write("This is a text file\n\n")
            for line in data:
                file.write(line + "\n")
