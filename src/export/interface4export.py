# Abstract base class
from abc import ABC, abstractmethod
from typing import List


class IExportAlgorithm(ABC):

    @abstractmethod
    def save_file(self, data: List[str], file_path: str):
        pass
