from .export_excel import ExportExcel
from .export_pdf import ExportPDF
from .export_txt import ExportTxt

from enum import Enum


class ChooseExport(Enum):
    EX_PDF: str = "EX_PDF"
    EX_EXCEL: str = "EX_EXCEL"
    EX_TXT: str = "EX_TXT"


# Factory class
class ExportAlgorithmFactory:

    @staticmethod
    def get_exporter(file_type: ChooseExport):
        if file_type == ChooseExport.EX_PDF:
            return ExportPDF()
        elif file_type == ChooseExport.EX_EXCEL:
            return ExportExcel()
        elif file_type == ChooseExport.EX_TXT:
            return ExportTxt()
        else:
            raise ValueError(f"Unknown file type: {file_type}")
