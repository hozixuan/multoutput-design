from .interface4export import IExportAlgorithm


# Concrete implementations
class ExportPDF(IExportAlgorithm):

    def save_file(self, data, filepath: str):
        with open(filepath, "w") as file:
            file.write("This is a pdf file\n\n")
            for line in data:
                file.write(line + "\n")
