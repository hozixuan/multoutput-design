from .interface4export import IExportAlgorithm


class ExportExcel(IExportAlgorithm):

    def save_file(self, data, filepath: str):
        with open(filepath, "w") as file:
            file.write("This is Excel!\n\n")
            for line in data:
                file.write(line + "\n")
