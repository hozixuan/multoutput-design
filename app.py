from src.misc import get_data  # mischellaneuos func to get data
from src.export import ChooseExport, ExportAlgorithmFactory


# Example usage
def main():
    ## The processing flow
    ## 1. Get data
    data = get_data("data/sample_data.txt")
    ## 2. Get Exporter
    pdf_exporter = ExportAlgorithmFactory.get_exporter(ChooseExport.EX_PDF)
    txt_exporter = ExportAlgorithmFactory.get_exporter(ChooseExport.EX_EXCEL)
    excel_exporter = ExportAlgorithmFactory.get_exporter(ChooseExport.EX_TXT)
    ## 3. Export the data
    pdf_exporter.save_file(data, "output/data.pdf")
    txt_exporter.save_file(data, "output/data.txt")
    excel_exporter.save_file(data, "output/data.csv")


if __name__ == "__main__":
    main()
