# Multiple Output Design

An app to output data into multiple different format.

## Assumption

1. The data source is always the same
2. Developer just need to add more algorithm on top of it

## Getting Start

Run the script with `python3 app.py`, need not install any dependency library. The app will generate `data.txt`, `data.pdf`, `data.csv` in folder `output/`
